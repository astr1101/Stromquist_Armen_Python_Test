"""Main script for generating output.csv."""
import pandas as pd
import numpy as np

def main():
    #Reading the Raw Original Data from CSV
    
    raw_data=pd.read_csv('./data/raw/pitchdata.csv')
    df=pd.DataFrame(raw_data, columns = ['GameId','PitcherId','HitterId','PitcherSide','HitterSide','PrimaryEvent','PitcherTeamId','HitterTeamId','PA','AB','H','2B','3B','HR','TB','BB','SF','HBP'])    
    
    #Selecting all Hitter ID's and Right Handed Pitching with PA>25
    
    hitter_id_rhp=df[df['PitcherSide']=='R'].groupby('HitterId')['PA','AB','H','2B','3B','HR','TB','BB','SF','HBP'].sum().copy()
    hitter_id_rhp=hitter_id_rhp[hitter_id_rhp['PA'] >= 25]
    
    #Selecting all Hitter ID's and Left Handed Pitching with PA>25
    
    hitter_id_lhp=df[df['PitcherSide']=='L'].groupby('HitterId')['PA','AB','H','2B','3B','HR','TB','BB','SF','HBP'].sum().copy()
    hitter_id_lhp=hitter_id_lhp[hitter_id_lhp['PA'] >= 25]
    
   
    labels=['PA','AB','H','2B','3B','HR','TB','BB','SF','HBP']
    
    #Creating AVG, OBP, SLG, OPS columns with their respective stats
    
    hitter_id_rhp['AVG']=round(hitter_id_rhp.H/hitter_id_rhp.AB,3)  
    hitter_id_rhp['OBP']=round((hitter_id_rhp.H+hitter_id_rhp.BB+hitter_id_rhp.HBP)/(hitter_id_rhp.AB+hitter_id_rhp.BB+hitter_id_rhp.HBP+hitter_id_rhp.SF),3)    
    hitter_id_rhp['SLG']=round(hitter_id_rhp.TB/hitter_id_rhp.AB, 3)
    hitter_id_rhp['OPS']=round((hitter_id_rhp.TB/hitter_id_rhp.AB)+(hitter_id_rhp.H+hitter_id_rhp.BB+hitter_id_rhp.HBP)/(hitter_id_rhp.AB+hitter_id_rhp.BB+hitter_id_rhp.HBP+hitter_id_rhp.SF),3)
    
    #Creating a column with vs RHP
    
    hitter_id_rhp['Split']='vs RHP'
    
    #Hitter ID Dataframe Against Right Handed Pitching and dropping the columns with header names from labels
    
    harhp=hitter_id_rhp.drop(labels, axis=1).copy()
     
    #Creating the combinations for HitterID vs RHP 
     
    for i in range(4):
        stats=['AVG', 'OBP', 'SLG', 'OPS'] 
        harhp['Stat']=str(stats[i])
        stat_item=stats.remove(stats[i])
        harhp['SubjectID']=hitter_id_rhp.index
        harhp['Subject']='HitterId'
        if(i==0):
            harhp_1=harhp.drop(stats, axis=1).copy()
        if(i==1):
            harhp_2=harhp.drop(stats, axis=1).copy()
        if(i==2):
            harhp_3=harhp.drop(stats, axis=1).copy()
        if(i==3):
            harhp_4=harhp.drop(stats, axis=1).copy()
        stats.append(stat_item)
    
    #Rearranging and concatenating dataframes together 
    
    result_values=pd.concat([harhp_1.AVG,harhp_2.OBP, harhp_3.SLG, harhp_4.OPS]) 
    result_sub_id=pd.concat([harhp_1.SubjectID,harhp_2.SubjectID, harhp_3.SubjectID, harhp_4.SubjectID])
    result_sub=pd.concat([harhp_1.Subject,harhp_2.Subject, harhp_3.Subject, harhp_4.Subject])
    result_split=pd.concat([harhp_1.Split,harhp_2.Split, harhp_3.Split, harhp_4.Split])
    result_stat=pd.concat([harhp_1.Stat,harhp_2.Stat, harhp_3.Stat, harhp_4.Stat])
    
    
    real_harhp=pd.concat([result_sub_id, result_stat, result_split, result_sub, result_values], axis=1, keys=['Subject Id','Stat','Split', 'Subject', 'Value'])
    
    
    hitter_id_lhp['AVG']=round(hitter_id_lhp.H/hitter_id_lhp.AB,3)  
    hitter_id_lhp['OBP']=round((hitter_id_lhp.H+hitter_id_lhp.BB+hitter_id_lhp.HBP)/(hitter_id_lhp.AB+hitter_id_lhp.BB+hitter_id_lhp.HBP+hitter_id_lhp.SF),3)    
    hitter_id_lhp['SLG']=round(hitter_id_lhp.TB/hitter_id_lhp.AB, 3)
    hitter_id_lhp['OPS']=round((hitter_id_lhp.TB/hitter_id_lhp.AB)+(hitter_id_lhp.H+hitter_id_lhp.BB+hitter_id_lhp.HBP)/(hitter_id_lhp.AB+hitter_id_lhp.BB+hitter_id_lhp.HBP+hitter_id_lhp.SF),3)
    hitter_id_lhp['Split']='vs LHP'

    #Hitter ID Against Left Handed Pitching
    halhp=hitter_id_lhp.drop(labels, axis=1).copy()
    
    for i in range(4):
        stats=['AVG', 'OBP', 'SLG', 'OPS'] 
        halhp['Stat']=str(stats[i])
        stat_item=stats.remove(stats[i])
        halhp['SubjectID']=hitter_id_lhp.index
        halhp['Subject']='HitterId'
        if(i==0):
            halhp_1=halhp.drop(stats, axis=1).copy()
        if(i==1):
            halhp_2=halhp.drop(stats, axis=1).copy()
        if(i==2):
            halhp_3=halhp.drop(stats, axis=1).copy()
        if(i==3):
            halhp_4=halhp.drop(stats, axis=1).copy()
        stats.append(stat_item)
    
    
    result_values_2=pd.concat([halhp_1.AVG,halhp_2.OBP, halhp_3.SLG, halhp_4.OPS]) 
    result_sub_id_2=pd.concat([halhp_1.SubjectID,halhp_2.SubjectID, halhp_3.SubjectID, halhp_4.SubjectID])
    result_sub_2=pd.concat([halhp_1.Subject,halhp_2.Subject, halhp_3.Subject, halhp_4.Subject])
    result_split_2=pd.concat([halhp_1.Split,halhp_2.Split, halhp_3.Split, halhp_4.Split])
    result_stat_2=pd.concat([halhp_1.Stat,halhp_2.Stat, halhp_3.Stat, halhp_4.Stat])
    
    real_halhp=pd.concat([result_sub_id_2, result_stat_2, result_split_2, result_sub_2, result_values_2], axis=1, keys=['Subject Id','Stat','Split', 'Subject', 'Value'])
    
    
    
    hitter_team_id=df[df['PitcherSide']=='R'].groupby('HitterTeamId')['PA','AB','H','2B','3B','HR','TB','BB','SF','HBP'].sum().copy()
    hitter_team_id=hitter_team_id[hitter_team_id['PA'] >= 25]
    hitter_team_id2=df[df['PitcherSide']=='L'].groupby('HitterTeamId')['PA','AB','H','2B','3B','HR','TB','BB','SF','HBP'].sum().copy()
    hitter_team_id2=hitter_team_id2[hitter_team_id2['PA'] >= 25]
    
    hitter_team_id['AVG']=round(hitter_team_id.H/hitter_team_id.AB,3)  
    hitter_team_id['OBP']=round((hitter_team_id.H+hitter_team_id.BB+hitter_team_id.HBP)/(hitter_team_id.AB+hitter_team_id.BB+hitter_team_id.HBP+hitter_team_id.SF),3)    
    hitter_team_id['SLG']=round(hitter_team_id.TB/hitter_team_id.AB, 3)
    hitter_team_id['OPS']=round((hitter_team_id.TB/hitter_team_id.AB)+(hitter_team_id.H+hitter_team_id.BB+hitter_team_id.HBP)/(hitter_team_id.AB+hitter_team_id.BB+hitter_team_id.HBP+hitter_team_id.SF),3)
    hitter_team_id['Split']='vs RHP'
    
    
    hitter_team_id2['AVG']=round(hitter_team_id2.H/hitter_team_id2.AB,3)  
    hitter_team_id2['OBP']=round((hitter_team_id2.H+hitter_team_id2.BB+hitter_team_id2.HBP)/(hitter_team_id2.AB+hitter_team_id2.BB+hitter_team_id2.HBP+hitter_team_id2.SF),3)    
    hitter_team_id2['SLG']=round(hitter_team_id2.TB/hitter_team_id2.AB, 3)
    hitter_team_id2['OPS']=round((hitter_team_id2.TB/hitter_team_id2.AB)+(hitter_team_id2.H+hitter_team_id2.BB+hitter_team_id2.HBP)/(hitter_team_id2.AB+hitter_team_id2.BB+hitter_team_id2.HBP+hitter_team_id2.SF),3)
    hitter_team_id2['Split']='vs LHP'
    
    
    #Hitter Team ID Against Right Handed Pitching
    htarhp=hitter_team_id.drop(labels, axis=1).copy()
    
    for i in range(4):
        stats=['AVG', 'OBP', 'SLG', 'OPS']  
        htarhp['Stat']=str(stats[i])
        stat_item=stats.remove(stats[i])
        htarhp['SubjectID']=hitter_team_id.index
        htarhp['Subject']='HitterTeamId'
        if(i==0):
            htarhp_1=htarhp.drop(stats, axis=1).copy()
        if(i==1):
            htarhp_2=htarhp.drop(stats, axis=1).copy()
        if(i==2):
            htarhp_3=htarhp.drop(stats, axis=1).copy()
        if(i==3):
            htarhp_4=htarhp.drop(stats, axis=1).copy()
        stats.append(stat_item)
        
    result_values_3=pd.concat([htarhp_1.AVG,htarhp_2.OBP, htarhp_3.SLG, htarhp_4.OPS]) 
    result_sub_id_3=pd.concat([htarhp_1.SubjectID,htarhp_2.SubjectID, htarhp_3.SubjectID, htarhp_4.SubjectID])
    result_sub_3=pd.concat([htarhp_1.Subject,htarhp_2.Subject, htarhp_3.Subject, htarhp_4.Subject])
    result_split_3=pd.concat([htarhp_1.Split,htarhp_2.Split, htarhp_3.Split, htarhp_4.Split])
    result_stat_3=pd.concat([htarhp_1.Stat,htarhp_2.Stat, htarhp_3.Stat, htarhp_4.Stat])  
        
    real_htarhp=pd.concat([result_sub_id_3, result_stat_3, result_split_3, result_sub_3, result_values_3], axis=1, keys=['Subject Id','Stat','Split', 'Subject', 'Value'])    
        
        
    #Hitter Team ID Against Left Handed Pitching
    htalhp=hitter_team_id2.drop(labels, axis=1).copy()
    
    for i in range(4):
        stats=['AVG', 'OBP', 'SLG', 'OPS']  
        htalhp['Stat']=str(stats[i])
        stat_item=stats.remove(stats[i])
        htalhp['SubjectID']=hitter_team_id2.index
        htalhp['Subject']='HitterTeamId'
        if(i==0):
            htalhp_1=htalhp.drop(stats, axis=1).copy()
        if(i==1):
            htalhp_2=htalhp.drop(stats, axis=1).copy()
        if(i==2):
            htalhp_3=htalhp.drop(stats, axis=1).copy()
        if(i==3):
            htalhp_4=htalhp.drop(stats, axis=1).copy()
        stats.append(stat_item)
    
    result_values_4=pd.concat([htalhp_1.AVG,htalhp_2.OBP, htalhp_3.SLG, htalhp_4.OPS]) 
    result_sub_id_4=pd.concat([htalhp_1.SubjectID,htalhp_2.SubjectID, htalhp_3.SubjectID, htalhp_4.SubjectID])
    result_sub_4=pd.concat([htalhp_1.Subject,htalhp_2.Subject, htalhp_3.Subject, htalhp_4.Subject])
    result_split_4=pd.concat([htalhp_1.Split,htalhp_2.Split, htalhp_3.Split, htalhp_4.Split])
    result_stat_4=pd.concat([htalhp_1.Stat,htalhp_2.Stat, htalhp_3.Stat, htalhp_4.Stat])  
        
    real_htalhp=pd.concat([result_sub_id_4, result_stat_4, result_split_4, result_sub_4, result_values_4], axis=1, keys=['Subject Id','Stat','Split', 'Subject', 'Value'])    
    
    
    
    
    pitcher_id_rhh=df[df['HitterSide']=='R'].groupby('PitcherId')['PA','AB','H','2B','3B','HR','TB','BB','SF','HBP'].sum().copy()
    pitcher_id_rhh=pitcher_id_rhh[pitcher_id_rhh['PA'] >= 25]
    
    pitcher_id_rhh['AVG']=round(pitcher_id_rhh.H/pitcher_id_rhh.AB,3)  
    pitcher_id_rhh['OBP']=round((pitcher_id_rhh.H+pitcher_id_rhh.BB+pitcher_id_rhh.HBP)/(pitcher_id_rhh.AB+pitcher_id_rhh.BB+pitcher_id_rhh.HBP+pitcher_id_rhh.SF),3)    
    pitcher_id_rhh['SLG']=round(pitcher_id_rhh.TB/pitcher_id_rhh.AB, 3)
    pitcher_id_rhh['OPS']=round((pitcher_id_rhh.TB/pitcher_id_rhh.AB)+(pitcher_id_rhh.H+pitcher_id_rhh.BB+pitcher_id_rhh.HBP)/(pitcher_id_rhh.AB+pitcher_id_rhh.BB+pitcher_id_rhh.HBP+pitcher_id_rhh.SF),3)
    
    pitcher_id_rhh['Split']='vs RHH'
    
    #Pitcher ID against Right Handed Hitters
    parhh=pitcher_id_rhh.drop(labels, axis=1).copy()
    
    for i in range(4):
        stats=['AVG', 'OBP', 'SLG', 'OPS']  
        parhh['Stat']=str(stats[i])
        stat_item=stats.remove(stats[i])
        parhh['SubjectID']=pitcher_id_rhh.index
        parhh['Subject']='PitcherId'
        if(i==0):
            parhh_1=parhh.drop(stats, axis=1).copy()
        if(i==1):
            parhh_2=parhh.drop(stats, axis=1).copy()
        if(i==2):
            parhh_3=parhh.drop(stats, axis=1).copy()
        if(i==3):
            parhh_4=parhh.drop(stats, axis=1).copy()
        stats.append(stat_item)
    
    result_values_5=pd.concat([parhh_1.AVG,parhh_2.OBP, parhh_3.SLG, parhh_4.OPS]) 
    result_sub_id_5=pd.concat([parhh_1.SubjectID,parhh_2.SubjectID, parhh_3.SubjectID, parhh_4.SubjectID])
    result_sub_5=pd.concat([parhh_1.Subject,parhh_2.Subject, parhh_3.Subject, parhh_4.Subject])
    result_split_5=pd.concat([parhh_1.Split,parhh_2.Split, parhh_3.Split, parhh_4.Split])
    result_stat_5=pd.concat([parhh_1.Stat,parhh_2.Stat, parhh_3.Stat, parhh_4.Stat])  
        
    real_parhh=pd.concat([result_sub_id_5, result_stat_5, result_split_5, result_sub_5, result_values_5], axis=1, keys=['Subject Id','Stat','Split', 'Subject', 'Value'])    
    
  
    
    pitcher_id_lhh=df[df['HitterSide']=='L'].groupby('PitcherId')['PA','AB','H','2B','3B','HR','TB','BB','SF','HBP'].sum().copy()
    pitcher_id_lhh=pitcher_id_lhh[pitcher_id_lhh['PA'] >= 25]
    
    pitcher_id_lhh['AVG']=round(pitcher_id_lhh.H/pitcher_id_lhh.AB,3)  
    pitcher_id_lhh['OBP']=round((pitcher_id_lhh.H+pitcher_id_lhh.BB+pitcher_id_lhh.HBP)/(pitcher_id_lhh.AB+pitcher_id_lhh.BB+pitcher_id_lhh.HBP+pitcher_id_lhh.SF),3)    
    pitcher_id_lhh['SLG']=round(pitcher_id_lhh.TB/pitcher_id_lhh.AB, 3)
    pitcher_id_lhh['OPS']=round((pitcher_id_lhh.TB/pitcher_id_lhh.AB)+(pitcher_id_lhh.H+pitcher_id_lhh.BB+pitcher_id_lhh.HBP)/(pitcher_id_lhh.AB+pitcher_id_lhh.BB+pitcher_id_lhh.HBP+pitcher_id_lhh.SF),3)
    pitcher_id_lhh['Split']='vs LHH'

    
    #Pitcher ID against Left Handed Hitters
    palhh=pitcher_id_lhh.drop(labels, axis=1).copy()
    
    
    for i in range(4):
        stats=['AVG', 'OBP', 'SLG', 'OPS']  
        palhh['Stat']=str(stats[i])
        stat_item=stats.remove(stats[i])
        palhh['SubjectID']=pitcher_id_lhh.index
        palhh['Subject']='PitcherId'
        if(i==0):
            palhh_1=palhh.drop(stats, axis=1).copy()
        if(i==1):
            palhh_2=palhh.drop(stats, axis=1).copy()
        if(i==2):
            palhh_3=palhh.drop(stats, axis=1).copy()
        if(i==3):
            palhh_4=palhh.drop(stats, axis=1).copy()
        stats.append(stat_item)
    
    result_values_6=pd.concat([palhh_1.AVG,palhh_2.OBP, palhh_3.SLG, palhh_4.OPS]) 
    result_sub_id_6=pd.concat([palhh_1.SubjectID,palhh_2.SubjectID, palhh_3.SubjectID, palhh_4.SubjectID])
    result_sub_6=pd.concat([palhh_1.Subject,palhh_2.Subject, palhh_3.Subject, palhh_4.Subject])
    result_split_6=pd.concat([palhh_1.Split,palhh_2.Split, palhh_3.Split, palhh_4.Split])
    result_stat_6=pd.concat([palhh_1.Stat,palhh_2.Stat, palhh_3.Stat, palhh_4.Stat])  
        
    real_palhh=pd.concat([result_sub_id_6, result_stat_6, result_split_6, result_sub_6, result_values_6], axis=1, keys=['Subject Id','Stat','Split', 'Subject', 'Value'])    
    

    
    pitcher_team_id_rhh=df[df['HitterSide']=='R'].groupby('PitcherTeamId')['PA','AB','H','2B','3B','HR','TB','BB','SF','HBP'].sum().copy()
    pitcher_team_id_rhh=pitcher_team_id_rhh[pitcher_team_id_rhh['PA'] >= 25]
    
    pitcher_team_id_rhh['AVG']=round(pitcher_team_id_rhh.H/pitcher_team_id_rhh.AB,3)  
    pitcher_team_id_rhh['OBP']=round((pitcher_team_id_rhh.H+pitcher_team_id_rhh.BB+pitcher_team_id_rhh.HBP)/(pitcher_team_id_rhh.AB+pitcher_team_id_rhh.BB+pitcher_team_id_rhh.HBP+pitcher_team_id_rhh.SF),3)    
    pitcher_team_id_rhh['SLG']=round(pitcher_team_id_rhh.TB/pitcher_team_id_rhh.AB, 3)
    pitcher_team_id_rhh['OPS']=round((pitcher_team_id_rhh.TB/pitcher_team_id_rhh.AB)+(pitcher_team_id_rhh.H+pitcher_team_id_rhh.BB+pitcher_team_id_rhh.HBP)/(pitcher_team_id_rhh.AB+pitcher_team_id_rhh.BB+pitcher_team_id_rhh.HBP+pitcher_team_id_rhh.SF),3)
    
    pitcher_team_id_rhh['Split']='vs RHH'

    #Pitcher Team against Right Handed Hitters
    ptarhh=pitcher_team_id_rhh.drop(labels, axis=1).copy()
    
    
    for i in range(4):
        stats=['AVG', 'OBP', 'SLG', 'OPS']  
        ptarhh['Stat']=str(stats[i])
        stat_item=stats.remove(stats[i])
        ptarhh['SubjectID']=pitcher_team_id_rhh.index
        ptarhh['Subject']='PitcherTeamId'
        if(i==0):
            ptarhh_1=ptarhh.drop(stats, axis=1).copy()
        if(i==1):
            ptarhh_2=ptarhh.drop(stats, axis=1).copy()
        if(i==2):
            ptarhh_3=ptarhh.drop(stats, axis=1).copy()
        if(i==3):
            ptarhh_4=ptarhh.drop(stats, axis=1).copy()
        stats.append(stat_item)
    
    result_values_7=pd.concat([ptarhh_1.AVG,ptarhh_2.OBP, ptarhh_3.SLG, ptarhh_4.OPS]) 
    result_sub_id_7=pd.concat([ptarhh_1.SubjectID,ptarhh_2.SubjectID, ptarhh_3.SubjectID, ptarhh_4.SubjectID])
    result_sub_7=pd.concat([ptarhh_1.Subject,ptarhh_2.Subject, ptarhh_3.Subject, ptarhh_4.Subject])
    result_split_7=pd.concat([ptarhh_1.Split,ptarhh_2.Split, ptarhh_3.Split, ptarhh_4.Split])
    result_stat_7=pd.concat([ptarhh_1.Stat,ptarhh_2.Stat, ptarhh_3.Stat, ptarhh_4.Stat])  
        
    real_ptarhh=pd.concat([result_sub_id_7, result_stat_7, result_split_7, result_sub_7, result_values_7], axis=1, keys=['Subject Id','Stat','Split', 'Subject', 'Value'])    
    
    
    pitcher_team_id_lhh=df[df['HitterSide']=='L'].groupby('PitcherTeamId')['PA','AB','H','2B','3B','HR','TB','BB','SF','HBP'].sum().copy()
    pitcher_team_id_lhh=pitcher_team_id_lhh[pitcher_team_id_lhh['PA'] >= 25]
    
    pitcher_team_id_lhh['AVG']=round(pitcher_team_id_lhh.H/pitcher_team_id_lhh.AB,3)  
    pitcher_team_id_lhh['OBP']=round((pitcher_team_id_lhh.H+pitcher_team_id_lhh.BB+pitcher_team_id_lhh.HBP)/(pitcher_team_id_lhh.AB+pitcher_team_id_lhh.BB+pitcher_team_id_lhh.HBP+pitcher_team_id_lhh.SF),3)    
    pitcher_team_id_lhh['SLG']=round(pitcher_team_id_lhh.TB/pitcher_team_id_lhh.AB, 3)
    pitcher_team_id_lhh['OPS']=round((pitcher_team_id_lhh.TB/pitcher_team_id_lhh.AB)+(pitcher_team_id_lhh.H+pitcher_team_id_lhh.BB+pitcher_team_id_lhh.HBP)/(pitcher_team_id_lhh.AB+pitcher_team_id_lhh.BB+pitcher_team_id_lhh.HBP+pitcher_team_id_lhh.SF),3)
    
    pitcher_team_id_lhh['Split']='vs LHH'
    
    
    #Pitcher Team against Left Handed Hitters
    ptalhh=pitcher_team_id_lhh.drop(labels, axis=1).copy()
    
    
    for i in range(4):
        stats=['AVG', 'OBP', 'SLG', 'OPS'] 
        ptalhh['Stat']=str(stats[i])
        stat_item=stats.remove(stats[i])
        ptalhh['SubjectID']=pitcher_team_id_lhh.index
        ptalhh['Subject']='PitcherTeamId'
        if(i==0):
            ptalhh_1=ptalhh.drop(stats, axis=1).copy()
        if(i==1):
            ptalhh_2=ptalhh.drop(stats, axis=1).copy()
        if(i==2):
            ptalhh_3=ptalhh.drop(stats, axis=1).copy()
        if(i==3):
            ptalhh_4=ptalhh.drop(stats, axis=1).copy()
        stats.append(stat_item)
    
   
    result_values_8=pd.concat([ptalhh_1.AVG,ptalhh_2.OBP, ptalhh_3.SLG, ptalhh_4.OPS]) 
    result_sub_id_8=pd.concat([ptalhh_1.SubjectID,ptalhh_2.SubjectID, ptalhh_3.SubjectID, ptalhh_4.SubjectID])
    result_sub_8=pd.concat([ptalhh_1.Subject,ptalhh_2.Subject, ptalhh_3.Subject, ptalhh_4.Subject])
    result_split_8=pd.concat([ptalhh_1.Split,ptalhh_2.Split, ptalhh_3.Split, ptalhh_4.Split])
    result_stat_8=pd.concat([ptalhh_1.Stat,ptalhh_2.Stat, ptalhh_3.Stat, ptalhh_4.Stat])  
        
    real_ptalhh=pd.concat([result_sub_id_8, result_stat_8, result_split_8, result_sub_8, result_values_8], axis=1, keys=['Subject Id','Stat','Split', 'Subject', 'Value'])    
   
    total_results=pd.concat([real_ptalhh, real_ptarhh, real_harhp, real_halhp, real_htarhp, real_htalhp, real_parhh, real_palhh]) 
   
    total_results.sort_values(['Subject Id', 'Stat', 'Split', 'Subject'], ascending=[True, True, True, True], inplace=True)
    
    total_results.to_csv('./data/processed/output.csv', index=False, header=True)
    
    
    pass

if __name__ == '__main__':
    main()
